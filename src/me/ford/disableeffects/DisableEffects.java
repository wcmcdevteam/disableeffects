package me.ford.disableeffects;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityPotionEffectEvent;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.potion.PotionEffectType;

import net.md_5.bungee.api.ChatColor;

public class DisableEffects extends JavaPlugin implements Listener {
	List<PotionEffectType> types = new ArrayList<PotionEffectType>();
	
	public void onEnable() {
		getServer().getPluginManager().registerEvents(this, this);
		loadConfiguration();
	}
	
	private void loadConfiguration() {
		getConfig().options().copyDefaults(true);
		saveConfig();
		getTypes();
	}
	
	public void getTypes() {
		types.clear();
		if (getConfig().contains("types")) {
			List<String> typelist = getConfig().getStringList("types");
			for (String typename : typelist) {
				PotionEffectType type = PotionEffectType.getByName(typename);
				if (type != null) {
					types.add(type);
				}
			}
		}
	}
	
	@EventHandler
	public void onEntityPotionEffectEvent(EntityPotionEffectEvent event) {
		if (event.getNewEffect() != null) {
			if (types.contains(event.getNewEffect().getType())) {
				event.setCancelled(true);
			}
		}
	}
	
	private void saveEffects() {
		List<String> stringList = types.stream().map(PotionEffectType::getName).collect(Collectors.toList());
		getConfig().set("types", stringList);
		saveConfig();
	}
	
	private boolean commandChangeEffects(CommandSender sender, String[] args, boolean disable) {
		if (args.length < 1) { return false; }
		PotionEffectType newtype = PotionEffectType.getByName(args[0].toUpperCase());
		if (newtype == null) { sender.sendMessage(ChatColor.RED + "Type not found: " + ChatColor.DARK_GRAY + args[0]); return true;}
		if (disable) {
			if (types.contains(newtype)) { sender.sendMessage(ChatColor.RED + "Already disabled!"); return true;}
			else { 
				types.add(newtype); 
				sender.sendMessage(ChatColor.AQUA + "Successfully disabled: " + ChatColor.DARK_GRAY + args[0]);
			}
		} else {
			if (!types.contains(newtype)) { sender.sendMessage(ChatColor.RED + "Already enabled!"); return true;}
			else { 
				types.remove(newtype);
				sender.sendMessage(ChatColor.AQUA + "Successfully re-enabled: " + ChatColor.DARK_GRAY + args[0]);
			}
		}
		saveEffects();
		return true;
	}
	
	public boolean onCommand(CommandSender sender, Command command, String label, String args[]) {
		if (command.getName().equalsIgnoreCase("disableeffectsreload")) {
			reloadConfig();
			loadConfiguration();
			return true;
		} else if (command.getName().equalsIgnoreCase("disableeffectsdisable")) {
			return commandChangeEffects(sender, args, true);
		} else if (command.getName().equalsIgnoreCase("disableeffectsenable")) {
			return commandChangeEffects(sender, args, false);
		}
		return false;
	}

}
